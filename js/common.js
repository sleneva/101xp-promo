$(document).ready(function() {
 
	$("#owl-demo").owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		navText: ["",""],
		singleItem: true,
		dotsContainer: '#carousel-custom-dots',
	});

	$('.owl-dot').click(function () {
	    $("#owl-demo").trigger('to.owl.carousel', [$(this).index(), 300]);
	});

	//Buttons 
	$( ".owl-prev" ).addClass( "btn_left" );
	$( ".owl-prev" ).addClass( "btn_next" );
 	
 	$( ".owl-next" ).addClass( "btn_right" );
	$( ".owl-next" ).addClass( "btn_next" );
		
});

